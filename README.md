Hex8 CPU

----------------------------------------------------------------------------------------------------

Description:

A working circuit design of a Hex8 CPU, with sample hex code to load into memory unit.

----------------------------------------------------------------------------------------------------

Pre-requisites:

ModuleSim (https://github.com/TeachingTechnologistBeth/ModuleSim/releases)

----------------------------------------------------------------------------------------------------